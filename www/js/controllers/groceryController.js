(function() {

app.controller('GroceryController', GroceryController);

function GroceryController($state, $rootScope,  $scope, sessionFactory, groceryFactory, $stateParams, API_URL, imageFactory, $timeout){
	let vm = this;
	
	vm.groceries = [];
  vm.checked = checked;
  vm.showGroceryImage = showGroceryImage;
  vm.doRefresh = doRefresh;
  vm.goToReceiptPage = goToReceiptPage;

  $scope.$on('$ionicView.enter', function(){
    if(!sessionFactory.session) {
      $state.go('login');
      return;
    }

  	groceryFactory.getGroceries().then(function(){
      vm.groceries = groceryFactory.groceries;
  	});
  })

  function doRefresh(){
    $timeout( function(){
      groceryFactory.getGroceries().then(function(){
        console.log(groceryFactory.groceries)
        vm.groceries = groceryFactory.groceries;
      });
      $scope.$broadcast('scroll.refreshComplete')
    }, 1000)
  }

  function goToReceiptPage(){
    $rootScope.$broadcast('nav-changes',{tabId:2})
    $state.go('receipt')
  }
  
  
  function checked(event, data){
    data.checked = event.target.checked;
    groceryFactory.updateGrocery(data);
  }

  function showGroceryImage(id){
    $state.go('image',{sort:'grocery',imageId: id});
  }
}

})();