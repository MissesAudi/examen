(function() {

app.controller('LoginController', LoginController);

function LoginController($state, $rootScope, sessionFactory){
	let vm = this;
	vm.email = '';
	vm.password = '';
	vm.errors = [];
	vm.loading = false;
	vm.user = {};
	$rootScope.loggedIn = false;
	vm.login = login;
	vm.session = sessionFactory.session;

	if(!vm.session){
		$state.go('login')	
	}else{
		$state.go('groceries')		
	}

	// sessionFactory.getSession().then(function(){
	// 	vm.session = sessionFactory.session;
	// });

	function login(email, password){
		vm.loading = true;
		vm.errorMessage = [];
	    if(!email){
	      vm.errorMessage.push("Je moet een email adres invullen");
	      vm.loading = false;
	    }else if(!password){
	      vm.errorMessage.push("Je moet een wachtwoord invoeren");
	      vm.loading = false;
	    }else{
	      sessionFactory.login(email, password).then(function(){
	        $rootScope.loggedIn = true;
	        $rootScope.hideHeader = false;
	        vm.user.email = email;
	        vm.user.password = password;
	        $state.go("groceries");
	      }, function(email, password){
          vm.loading = false;
	        if(email != vm.email || password != vm.password){
	          vm.errorMessage.push("emailadres of wachtwoord is onjuist");
	          vm.password = "";
	        }else{
	          vm.errorMessage.push("onbekende fout.");
	        }
	      });
	    }
	}
  	
}

})();