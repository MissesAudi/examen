(function() {

app.controller('AddGroceryController', AddGroceryController);

function AddGroceryController($state, $stateParams, groceryFactory, sessionFactory, API_URL){
	let vm = this;
	
	vm.grocery = [];
	vm.groceryTrue = false;
	vm.showImage = false;

  vm.back = back;
	vm.captureImage = captureImage;
	vm.saveGrocery = saveGrocery;
	vm.updateGrocery = updateGrocery;
  vm.key = key;

  if($stateParams && $stateParams.groceryId) {
    groceryFactory.getGrocery($stateParams.groceryId).then(function(){
      vm.grocery = groceryFactory.grocery;
      vm.groceryTrue = true;
      if(vm.grocery.image){
        vm.showImage = true;
        var image = document.getElementById('capturedImage');
        image.src =  'http://192.168.178.165/storage'+  vm.grocery.image.path;
      }
    })
  }


  document.addEventListener("deviceready", onDeviceReady, false);
  function onDeviceReady() {
    vm.cameraSettings = { 
        sourceType : Camera.PictureSourceType.CAMERA,
        correctOrientation: true,
        quality: 100,
        targetWidth: 600,
        destinationType: Camera.DestinationType.FILE_URI,
        encodingType: Camera.EncodingType.PNG,
        saveToPhotoAlbum:false
     };
  }

  function back(){
    javascript:history.go(-1);
  }

  function updateGrocery(grocery){
    if(!vm.grocery.file){
      groceryFactory.updateGrocery(grocery);
      $state.go('image', {sort:'grocery',imageId:grocery.id});
    }else{
      var url = encodeURI( API_URL + '/image?token=' + sessionFactory.session.token);
      var options = new FileUploadOptions();
      options.fileKey = "file";
      options.fileName = vm.grocery.file.substr(vm.grocery.file.lastIndexOf('/')+1);
      options.mimeType = "image/png";

      var headers={'headerParam':'headerValue', 'headerParam2':'headerValue2'};

      options.headers = headers;

      var ft = new FileTransfer();

      function win(r) {
          var response = JSON.parse(r.response);
          grocery.image_id = response.id;
          groceryFactory.updateGrocery(grocery);
          $state.go('image', {sort:'grocery',imageId:grocery.id});
          console.log('response', r)
          console.log("Code = " + r.responseCode);
          console.log("Response = " + r.response);
          console.log("Sent = " + r.bytesSent);
      }

      function fail(error) {
        console.log(error);
        alert("An error has occurred: Code = " + error.code);
        console.log("upload error source " + error.source);
        console.log("upload error target " + error.target);
      }

      ft.upload(vm.grocery.file, url, win, fail, options);
    }
  }

	function saveGrocery(grocery){
  	vm.errorMessage = [];
    if(!vm.grocery.name){
      vm.errorMessage.push("Je moet een boodschap invullen");
    }else{
      if(!vm.grocery.file){
         groceryFactory.saveGrocery(grocery);
      }else{
        var url = encodeURI( API_URL + '/image?token=' + sessionFactory.session.token);
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = vm.grocery.file.substr(vm.grocery.file.lastIndexOf('/')+1);
        options.mimeType = "image/png";

        var headers={'headerParam':'headerValue', 'headerParam2':'headerValue2'};

        options.headers = headers;

        var ft = new FileTransfer();

        function win(r) {
          var response = JSON.parse(r.response);
          grocery.image_id = response.id;
          groceryFactory.saveGrocery(grocery);
            console.log('response', r)
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
        }

        function fail(error) {
          console.log(error);
            alert("An error has occurred: Code = " + error.code);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
        }

        ft.upload(vm.grocery.file, url, win, fail, options);
      }
    }
	}

	function captureImage(){
		vm.showImage = true;
		if(navigator.camera){
      console.log(vm.onSuccess)
			navigator.camera.getPicture(vm.onSuccess, vm.onFail, vm.cameraSettings);

		}else{
     		alert("camera not found");
    }
  }

  vm.newImage = function(){
  	vm.showImage = false;
  }

  vm.onSuccess = function(imgUri){
    vm.showImage = true;
    vm.grocery.file = imgUri;
  }

  vm.onFail = function (message) {
     console.log('Camera canceld')
  }

  function key(event, grocery){
    if(event.which === 13){
      if(vm.groceryTrue === true){
        updateGrocery(grocery)
      }else{
        saveGrocery(grocery);
      }
    }else{
      return false;
    }
  }
}

})();