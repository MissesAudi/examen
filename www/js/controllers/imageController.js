(function() {

app.controller('ImageController', ImageController);

function ImageController($state, $stateParams, $rootScope, groceryFactory, receiptFactory, $timeout, $scope){
	let vm = this;
	vm.image = [];
	vm.receipt = false;

	vm.back = back;
	vm.deleteGrocery = deleteGrocery;
	vm.deleteReceipt = deleteReceipt;
	vm.goToReceipt = goToReceipt;
	vm.goToGrocery = goToGrocery;
	vm.doRefresh = doRefresh;
	vm.openImageViewer =  openImageViewer;

	if($stateParams.sort === 'receipt'){
		receiptFactory.getReceipt($stateParams.imageId).then(function(){
	      vm.image = receiptFactory.receipt;
	      vm.receipt = true;
	    })
	}

	if($stateParams.sort === 'grocery'){
	    groceryFactory.getGrocery($stateParams.imageId).then(function(){
	      vm.image = groceryFactory.grocery;
	    })
	}

  	function doRefresh(){
		$timeout( function(){
			if($stateParams.sort === 'receipt'){
				receiptFactory.getReceipt($stateParams.imageId).then(function(){
			      vm.image = receiptFactory.receipt;
			      vm.receipt = true;
			    })
			}

			if($stateParams.sort === 'grocery'){
			    groceryFactory.getGrocery($stateParams.imageId).then(function(){
			      vm.image = groceryFactory.grocery;
			    })
			}
		  $scope.$broadcast('scroll.refreshComplete')
		}, 1000)
	}
  

	function back(){
		javascript:history.go(-1);
	}
	
	function goToReceipt(id){
		$state.go('addReceipt',{receiptId: id});
	}

	function goToGrocery(id){
    	$state.go('addGrocery', {groceryId: id})
  	}

  	function deleteGrocery(id){
  		console.log(id)
  		var isConfirm = confirm('Wil je deze boodschap verwijderen?');
  		if(isConfirm){
  			groceryFactory.deleteGrocery(id);
  		}else{
  			return false;
  		}
  	}

  	function deleteReceipt(id){
  		var isConfirm = confirm('Wil je dit bonnetje verwijderen?');
  		if(isConfirm){
  			receiptFactory.deleteReceipt(id);
  		} else{
  			return false;
  		}
  	}

  	function openImageViewer(img){
  		var options = {
		    share: true, // default is false
		    closeButton: false, // default is true
		    copyToReference: true // default is false
		};

  		PhotoViewer.show('http://192.168.178.165/storage' + img, 'Optional Title', options);
  		
  	}
  }

})();