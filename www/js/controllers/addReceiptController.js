(function() {

app.controller('AddReceiptController', AddReceiptController);

function AddReceiptController($state, $stateParams, receiptFactory, API_URL, sessionFactory){
	let vm = this;
	vm.receipt = [];
	vm.showImage = false;
	vm.receiptTrue = false;

	vm.back = back;
	vm.captureImage = captureImage;
	vm.saveReceipt = saveReceipt;
	vm.update = update;
	vm.key = key;

	if($stateParams && $stateParams.receiptId) {
    receiptFactory.getReceipt($stateParams.receiptId).then(function(){
      vm.receipt = receiptFactory.receipt;
      vm.receiptTrue = true;
      vm.showImage = true;
      if(vm.receipt.image){
	      var image = document.getElementById('capturedImage');
	      image.src = 'http://192.168.178.165/storage' + vm.receipt.image.path;
      }
    })
  }

  document.addEventListener("deviceready", onDeviceReady, false);
  function onDeviceReady() {
    vm.cameraSettings = { 
          sourceType : Camera.PictureSourceType.CAMERA,
          correctOrientation: true,
          quality: 100,
          targetWidth: 600,
          destinationType: Camera.DestinationType.FILE_URL,
          encodingType: Camera.EncodingType.PNG,
          saveToPhotoAlbum:false
     };
  }

  	function back(){
		javascript:history.go(-1);
  	}

	function update(receipt){
		if(!vm.receipt.file){
			receiptFactory.updateReceipt(receipt);
			$state.go('image', {sort:'receipt',imageId:receipt.id});
		}else{
	    var url = encodeURI( API_URL + '/image?token=' + sessionFactory.session.token);
	    var options = new FileUploadOptions();
	    options.fileKey = "file";
	    options.fileName = vm.receipt.file.substr(vm.receipt.file.lastIndexOf('/')+1);
	    options.mimeType = "image/png";

	    var headers={'headerParam':'headerValue', 'headerParam2':'headerValue2'};

	    options.headers = headers;

	    var ft = new FileTransfer();

	    function win(r) {
	      var response = JSON.parse(r.response);
	      receipt.image_id = response.id;
			receiptFactory.updateReceipt(receipt);
			$state.go('image', {sort:'receipt',imageId:receipt.id});
			console.log('response', r)
			console.log("Code = " + r.responseCode);
			console.log("Response = " + r.response);
			console.log("Sent = " + r.bytesSent);
	    }

	    function fail(error) {
	      console.log(error);
	        alert("An error has occurred: Code = " + error.code);
	        console.log("upload error source " + error.source);
	        console.log("upload error target " + error.target);
	    }

	    ft.upload(vm.receipt.file, url, win, fail, options);
	    
		}
	}

  function saveReceipt(receipt){
	vm.errorMessage = [];
    if(!vm.receipt.name){
      vm.errorMessage.push("Je moet een naam invullen");
    }else if(!vm.receipt.file){
    	vm.errorMessage.push('Je moet nog een foto maken van het bonnetje');
    }else{
      var url = encodeURI( API_URL + '/image?token=' + sessionFactory.session.token);
      var options = new FileUploadOptions();
      options.fileKey = "file";
      options.fileName = vm.receipt.file.substr(vm.receipt.file.lastIndexOf('/')+1);
      options.mimeType = "image/png";

      var headers={'headerParam':'headerValue', 'headerParam2':'headerValue2'};

      options.headers = headers;

      var ft = new FileTransfer();

      function win(r) {
        var response = JSON.parse(r.response);
        receipt.image_id = response.id;
       	receiptFactory.saveReceipt(receipt);
          console.log('response', r)
          console.log("Code = " + r.responseCode);
          console.log("Response = " + r.response);
          console.log("Sent = " + r.bytesSent);
      }

      function fail(error) {
        console.log(error);
          alert("An error has occurred: Code = " + error.code);
          console.log("upload error source " + error.source);
          console.log("upload error target " + error.target);
      }

      ft.upload(vm.receipt.file, url, win, fail, options);
    }
	}

	function captureImage(){
		vm.showImage = true;
		if(navigator.camera){
			navigator.camera.getPicture(vm.onSuccess, vm.onFail, vm.cameraSettings);
		}else{
	   		alert("camera not found");
	  }
	}

	vm.newImage = function(){
		vm.showImage = false;
	}

	vm.onSuccess = function(imgUri){
		vm.receipt.file = imgUri;
	}

	vm.onFail = function (message) {
	  console.log('Camera ended');
	}

  	function key(event, receipt){
	    if(event.which === 13){
	    	if(vm.receiptTrue === true){
	    		update(receipt);
	    	}else{
	      		saveReceipt(receipt);
	    	}
	    }else{
	      return false;
	    }
  	}
}

})();