(function() {

app.controller('RegisterController', RegisterController);

function RegisterController(API_URL, $q, $http, $window, $location, registerFactory){
	let vm = this;
	vm.errors = [];
	vm.info = []; 

	vm.register = register;

	function register(user){
		vm.errorMessage = [];
    if(!vm.info.email){
      vm.errorMessage.push("Je moet een email adres invullen");
    }else if(!vm.info.password){
      vm.errorMessage.push("Je moet een wachtwoord invoeren");
    }else if(!vm.info.password2){
      vm.errorMessage.push("Je moet het wachtwoord nog herhalen");
    }else if(vm.info.password != vm.info.password2){
      vm.errorMessage.push("De wachtwoorden komen niet overeen");
    }else if(!vm.info.name){
      vm.errorMessage.push("Je moet een naam invoeren");
    }else{
      registerFactory.register(user);
    }
	}
}

})();