(function() {

app.controller('NavController', NavController);

function NavController($scope, registerFactory, sessionFactory, $state){
  let vm = this;
  vm.tab = 1;

  $scope.$on('nav-changes', function(event, id){
    let tabId = id.tabId;
    vm.setTab(tabId);
    vm.isSet(tabId)
  })

  vm.setTab = function (tabId) {
      vm.tab = tabId;
  };

  vm.isSet = function (tabId) {
      return vm.tab === tabId;
  };

  vm.logout = function(){
    $state.go('login')
  	sessionFactory.logout();
  }
}

})();