(function() {

app.controller('ReceiptController',ReceiptController);

function ReceiptController($state, $scope, $rootScope,sessionFactory, receiptFactory, userFactory, $stateParams, API_URL, $timeout){
	let vm = this;
	
	vm.receipts = [];

	vm.checked = checked;
	vm.showReceiptImage = showReceiptImage; 
	vm.goToReceipt = goToReceipt;
	vm.userLoggedIn = sessionFactory.email;
	vm.doRefresh = doRefresh;
	vm.goToGroceryPage = goToGroceryPage;

	$scope.$on('$ionicView.enter', function(){	
		if(!sessionFactory.session) {
			$state.go('login');
			return;
		}

		userFactory.getUser().then(function(){
			vm.user = userFactory.user;
		})

		receiptFactory.getReceipts().then(function(){
			vm.receipts = receiptFactory.receipts;
		})
	})

	function doRefresh(){
		$timeout( function(){
			receiptFactory.getReceipts().then(function(){
				vm.receipts = receiptFactory.receipts;
			})
			$scope.$broadcast('scroll.refreshComplete')
		}, 1000)
	}

	function goToGroceryPage(){
		$rootScope.$broadcast('nav-changes',{tabId:1})
		$state.go('groceries');
	}
	
  	function checked(event, data){
	    data.checked = event.target.checked;
	    receiptFactory.updateReceipt(data);
  	}
	function goToReceipt(id){
		$state.go('addReceipt',{receiptId: id});
	}

	function showReceiptImage(id){
		$state.go('image',{sort:'receipt',imageId: id})
	}
}

})();