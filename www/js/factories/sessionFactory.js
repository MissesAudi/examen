(function() {
  
"use strict";

app.factory('sessionFactory', sessionFactory);

function sessionFactory(API_URL,$http, $q, $window, $state) {
  let factory = {
    login : login,
	session: JSON.parse(localStorage.getItem('session')),
	logout : logout,
	getSession: getSession
  };

  return factory;


  function login(email, password){
    var d = $q.defer();
		$http.post(API_URL + '/user/login', {email: email, password: password}).then(function(res) {
	      factory.session = {
	      	token:res.data.token,
	      	isLoggedIn: true,
	      	email: email
	      };
	      localStorage.session = JSON.stringify(factory.session);
	      d.resolve();
	    }, d.reject);

    	return d.promise;
	}

	function getSession(){
		factory.session = JSON.parse(localStorage.getItem('session'))
	}

	function logout(){
		$window.location.reload();
		localStorage.clear();
	}

}

})();