(function() {
  
"use strict";

app.factory('userFactory', userFactory);

function userFactory(API_URL,$http, $q, sessionFactory) {
  let factory = {
    getUser:getUser,
    user:[]
  };

  return factory;

  function getUser(){
  	var d = $q.defer();
  	$http.get(API_URL + '/users?token=' + sessionFactory.session.token).then(success,error);

  	function success(res){
  		factory.user = res.data;
  		d.resolve(factory.user);
  	}

  	function error(error){
  		d.reject(error);
  	}

  	return d.promise;
  }
  

}

})();