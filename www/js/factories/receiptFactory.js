(function() {
  
"use strict";

app.factory('receiptFactory', receiptFactory);

function receiptFactory(API_URL,$http, $q, $window, $state, sessionFactory) {
  let factory = {
    getReceipts:getReceipts,
    getReceipt:getReceipt,
    saveReceipt:saveReceipt,
    updateReceipt:updateReceipt,
    deleteReceipt:deleteReceipt,
    receipts: [],
    receipt:[]
  };

  return factory;

  function getReceipt(id){
    var d = $q.defer();
    
    $http.get(API_URL + '/receipt/' + id + '?token=' + sessionFactory.session.token).then(success, error);

    function success(res){
      factory.receipt = res.data;
      d.resolve(factory.receipt);
    }

    function error(error){
      d.reject(error.data);
    }

    return d.promise;
  }

  function getReceipts(){
    var d = $q.defer();
    $http.get(API_URL + "/receipt?token=" + sessionFactory.session.token).then(function(res){
      factory.receipts = res.data;
      d.resolve(factory.receipts);
    }, function(error){
      d.reject();
    });
    
    return d.promise;
  }

  function saveReceipt(data){
    var d = $q.defer();
    $http.post(API_URL + '/receipt' + "?token=" + sessionFactory.session.token, {
    	"name":data.name,
    	"description":data.description,
    	"image_id":data.image_id
    }).then(success, error)

    function success(res){
      d.resolve(res.data);
      $state.go('receipt');      
    }

    function error(error){
      d.reject(error.data);
    }

    return d.promise;
  }

  function updateReceipt(data){
     return $http.put(API_URL + '/receipt/' + data.id + "?token=" + sessionFactory.session.token, data);
  }

  function deleteReceipt(id){
    $state.go('receipt');
    return $http.delete(API_URL + '/receipt/' + id + '?token=' + sessionFactory.session.token);
  }
}

})();