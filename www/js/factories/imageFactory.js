(function() {
  
"use strict";

app.factory('imageFactory', imageFactory);

function imageFactory($q, $http, API_URL, sessionFactory) {
  let factory = {
    getImage:getImage,
    image: []
  };

  return factory;

  function getImage(id){
    var d = $q.defer();

    $http.get(API_URL + '/image/' + id + "?token=" + sessionFactory.session.token).then(success, error)
    function success(res){
      factory.image = res.data;
      d.resolve(factory.image);
    }

    function error(error){
      d.reject(error.data);
    }

    return d.promise;
  }
}

})();