(function() {
  
"use strict";

app.factory('registerFactory', registerFactory);

function registerFactory(API_URL,$http, $q, $window, $state) {
  let factory = {
    register : register,
  };

  return factory;

  function register(user){
    var d = $q.defer();
    
    $http.post(API_URL + '/user/register', {
      'name': user.name,
      'email': user.email,
      'password': user.password
    }).then(success, error);

    function success(res){
      d.resolve(res.data);
      $state.go('login')
    }

    function error(error){
      d.reject(error.data);
    }

    return d.promise;
	}
}

})();