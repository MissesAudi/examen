(function() {
  
"use strict";

app.factory('groceryFactory', groceryFactory);

function groceryFactory(API_URL,$http, $q, $window, $state, sessionFactory) {
  let factory = {
    getGrocery: getGrocery,
    getGroceries: getGroceries,
    saveGrocery:saveGrocery,
    updateGrocery:updateGrocery,
    deleteGrocery:deleteGrocery,
    groceries: []
  };

  return factory;

  function getGrocery(id){
    var d = $q.defer();
    $http.get(API_URL + '/groceries/' + id + "?token=" + sessionFactory.session.token).then(success, error);

    function success(res){
      factory.grocery = res.data;
      d.resolve(factory.grocery);
    }

    function error(error){
      d.reject(error.data);
    }

    return d.promise;
  }

  function getGroceries(){
    var d = $q.defer();
    $http.get(API_URL + "/groceries?token=" + sessionFactory.session.token).then(function(res){
      factory.groceries = res.data;
      d.resolve(factory.groceries);
    }, function(error){
      d.reject();
    });
    
    return d.promise;
  }

  function saveGrocery(data){
    var d = $q.defer();
    console.log(data)

    $http.post(API_URL + "/groceries?token=" + sessionFactory.session.token, {
      "name": data.name,
      "description": data.description,
      "image_id": data.image_id
    }).then(success, error)

    function success(res){
      d.resolve(res.data);
      $state.go('groceries');
    }

    function error(error){
      d.reject(error.data);
    }

    return d.promise;

  }

  function updateGrocery(data){
    return $http.put(API_URL + '/groceries/' + data.id + "?token=" + sessionFactory.session.token, data);
  }

  function deleteGrocery(id){
    $state.go('groceries')
    return $http.delete(API_URL + '/groceries/' + id + '?token=' + sessionFactory.session.token);
  }
}

})();