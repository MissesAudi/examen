var app = angular.module('app', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.constant('API_URL', 'http://192.168.178.165/api')

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller:'LoginController',
    controllerAs:'vm'
  })

  .state('register',{
    url: '/register',
    templateUrl: 'templates/register.html',
    controller:'RegisterController',
    controllerAs:'vm'
  })

  .state('groceries', {
    url: '/groceries',
    templateUrl: 'templates/grocery.html',
    controller:'GroceryController',
    controllerAs:'vm'
  })

  .state('addGrocery', {
    url: '/addGrocery?groceryId',
    templateUrl: 'templates/addGrocery.html',
    controller:'AddGroceryController',
    controllerAs:'vm'
  })

  .state('receipt',{
    url: '/receipt',
    templateUrl: 'templates/receipt.html',
    controller:'ReceiptController',
    controllerAs:'vm'
  })

  .state('addReceipt',{
    url: '/addReceipt?receiptId',
    templateUrl: 'templates/addReceipt.html',
    controller:'AddReceiptController',
    controllerAs:'vm'
  })

  .state('image',{
    url: '/image?sort?imageId',
    templateUrl:'templates/image.html',
    controller:'ImageController',
    controllerAs:'vm'
  })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
